<?php

ini_set('display_error', 1);
ini_set('display_errors', 1);
error_reporting(E_ALL);

require 'vendor/autoload.php';

require 'testauth.php';

// create Shooju Connection
$sj = new \Shooju\Connection($SERVER, $USER, $API_KEY);

$series_id = 'your\\series\\id';

// // get an existing field, replace with series_id and field
echo $sj->get_field($series_id,'description')."\n";

// // get all fields
echo $sj->get_fields($series_id)['description']."\n";

// // get some fields
echo print_r($sj->get_fields($series_id, array('description','unit')))."\n";

// // get some points
echo print_r($sj->get_points($series_id, 'MAX', 'MIN', 2))."\n";

// // get a point
echo print_r($sj->get_point($series_id, '2009-01-01T00:00:00'))."\n";

// search
echo print_r($sj->search($series_id, ["fields"=>["description"], "page"=>1, "per_page"=>20]))."\n";

// scroll
echo print_r($sj->scroll($series_id, ["description"], 'MIN', 'MAX', 2))."\n";

?>
