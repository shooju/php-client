# shooju

_shooju_ is the official PHP client library for [Shooju](http://shooju.com) with the following features:

  - Authentication via username and api key
  - Getting series and fields

# Installation

The recommended way to install Shooju is with [Composer](https://getcomposer.org). Composer is a dependency management tool for PHP that allows you to declare the dependencies your project needs and installs them into your project.  To install Composer:


    :::sh
    curl -sS https://getcomposer.org/installer | php


You can add Shooju as a dependency using the composer.phar CLI:

    :::sh
    php composer.phar require shooju/shooju

Alternatively, you can specify Shooju as a dependency in your project's existing composer.json file:

    :::json
    {
       "require": {
          "shooju/shooju": "1.*"
       }
    }


# Basic Usage

    :::php
    // load composer libs
    require 'vendor/autoload.php';

    // create Shooju Connection
    $sj = new \Shooju\Connection($SERVER, $USER, $API_KEY);

    // get an existing field, replace with series_id and field
    echo $sj->get_field('BP\\carbon dioxide emissions\\Greece','description')."\n";

    // get all fields
    echo $sj->get_fields('BP\\carbon dioxide emissions\\Greece')['description']."\n";

    // get some fields
    echo print_r($sj->get_fields('BP\\carbon dioxide emissions\\Greece', ['description','unit']))."\n";

    // get a point
    echo print_r($sj->get_point('BP\\carbon dioxide emissions\\Greece', '2014-01-01'))."\n";

    // get many points
    echo print_r($sj->get_points('BP\\carbon dioxide emissions\\Greece', '2014-01-01', '2018-01-01', 100))."\n";

    // scroll, use for getting all results
    echo print_r($sj->scroll('Greece', ["description"], 'MIN', 'MAX', 10))."\n";

    // search, use for paginated field-only results
    echo print_r($sj->search('Greece', ["fields"=>["description"], "page"=>1, "per_page"=>20]))."\n";

    // raw api calls
    echo print_r($sj->raw->get('status'))."\n";

    
# Tutorial
## Connecting to Shooju

The first step when working with _shooju_ is to connect to Shooju using your **username** and **API key** (find it in the accounts section of [Shooju.com](http://shooju.com)). The first parameter is the full _shooju_ server url:

    :::php
    $sj = new \Shooju\Connection($SERVER, $USER, $API_KEY);


## Shooju Series Representation

The basic data building block in Shooju is the **series** (i.e. time series), and each **series** is identified by a **series id**.  A **series id** is a path-like string delimited by **\\** characters.  The path helps keep data series organized into folder-like structures.  By default, each user can write into the id space **users\\your_username\\\* **.  So if I'm Sam and I want to import my GDP forecasts, I might use the series id **users\\sam\\china\\gdp**.


## Getting Data

To get field values:

    :::php
    echo $sj->get_field('BP\\carbon dioxide emissions\\Greece','description');


To get all of the fields for a given series\_id as a array:

    :::php
    $fields_array = $sj->get_fields('BP\\carbon dioxide emissions\\Greece');


To get some of the fields under given series\_id:
    
    :::php
    $fields_array = $sj->get_fields('BP\\carbon dioxide emissions\\Greece', array("description", "unit"));

To get a point for a given series\_id and date (ISO-format, no timezone), use:
    
    :::php
    $point = $sj->get_point('BP\\carbon dioxide emissions\\Greece', '2014-01-01');

To get many points for a given series\_id between two dates (ISO-format, or 'MIN' / 'MAX'):
    
    :::php
    $points = $sj->get_point('BP\\carbon dioxide emissions\\Greece', '2014-01-01', 'MAX', 30);

To find series using a search query:

    :::php
    $series = $sj->search('query terms', ["fields"=>["description"], "page"=>1, "per_page"=>20]);

To get points and fields for series matching a search query:

    :::php
    $series = $sj->scroll('query terms', ['two','fields'], '2014-01-01', 'MAX', 30);

To use the raw API:

    :::php
    $res = $sj->raw->get('status'); //raw-> also supports post and delete
