<?php

namespace Shooju;

use GuzzleHttp;

//require 'vendor/autoload.php'; //for guzzle

class ApiException extends \Exception
{
    protected $code = '';
    public function __construct($message, $code = '') {
        parent::__construct($message);
        $this->code = $code;
    }
}

class Raw {

    private $guzzler;
    private $funcs = ['get','post','delete'];

    public function __construct($server, $user, $api_key)
    {
        $this->guzzler = new GuzzleHttp\Client([
            'base_url' => $server.'/api/1/',
            'defaults' => [
                'auth'    => [$user, $api_key],
            ]
        ]);
    }

    private function check_errors($json)
    {
        if($json['success']==false){
            throw new ApiException($json['error'] . ($json['description'] ? ' ' . $json['xxx'] : ''), $json['error']);
        }
    }

    public function __call($name, $args)
    {
        if (in_array($name, $this->funcs)) {
            $r = call_user_func_array(array($this->guzzler, $name), $args)->json();
            $this->check_errors($r);
            return $r;
        } else {
            throw new \BadMethodCallException($name . ' not implemented');
        }
    }
}

class Connection
{

    public $raw;
    
    public function __construct($server, $user, $api_key)
    {
        $this->raw = new Raw($server, $user, $api_key);
    }

    public function get_fields($series_id, $fields=array("*"))
    {
        $json = $this->raw->get('series', ['query' => [
            'max_points' => '0',
            'fields' => join(',',$fields),
            'series_id' => $series_id
        ]]);

        return $json['series'][0]['fields'];
    }

    public function get_field($series_id, $field)
    {
        $json = $this->raw->get('series', ['query' => [
            'max_points' => '0',
            'fields' => $field,
            'series_id' => $series_id
        ]]);
        
        return $json['series'][0]['fields'][$field];
    }

    public function get_points($series_id, $date_start="MIN", $date_finish="MAX", $max_points=10)
    {
        $json = $this->raw->get('series', ['query' => [
            'max_points' => $max_points,
            'df' => $date_start,
            'dt' => $date_finish,
            'date_format' => 'iso',
            'fields' => '',
            'series_id' => $series_id
        ]]);

        return $json['series'][0]['points'];
    }

    public function get_point($series_id, $dt)
    {
        $pts = $this->get_points($series_id, $dt, $dt, 1);
        if(count($pts)==1){
            return $pts[0];
        } else {
            return null;
        }
    }

    public function scroll($query, $fields=[], $date_start="MIN", $date_finish="MAX", $max_points=0, $sort=""){
        $request_args = [
            'query' => $query,
            'fields' => implode(',', $fields),
            'df' => $date_start,
            'dt' => $date_finish,
            'max_points' => $max_points,
            'sort' => $sort,
            'scroll_batch_size' => 100,
            'date_format' => 'iso',
            'scroll' => 'y'
        ];
        $serieses = [];
        $res = $this->raw->get('series', ["query" => $request_args]);
        $scroll_id = $res['scroll_id'];
        while(true){
            // if we didn't get any series, stop scrolling
            if(count($res['series'])==0)
                break;

            // process results
            for($s=0;$s<count($res['series']);$s++){
                array_push($serieses, $res['series'][$s]);
            }

            // get next scroll
            $res = $this->raw->get('series', ["query" => ['scroll_id'=>$scroll_id]]);
        }
        return $serieses;

    }

    public function search($query, $options){

        $request_args = ["query" => $query, 'max_points' => 0];

        if (array_key_exists("fields", $options)){
            $request_args["fields"] = implode(",", $options["fields"]);
        };


        foreach (["per_page", "page", "sort"] as $key) {
            if (array_key_exists($key, $options)){
                $request_args[$key] = $options[$key];
            };
            
        }

        $json = $this->raw->get('series', ["query" => $request_args]);

        return $json;

    }

}

?>
